<?php

Route::get('/', function(){

	$menu = Config::get('navbar.pages');

	return View::make('menu')->with('menu', $menu);
});

Route::get('/contact', function(){
	$menu = Config::get('navbar.pages');
    return View::make('contact')->with('menu', $menu);
});

Route::post('/contact', function(){
	$menu = Config::get('navbar.pages');
	$contact = new Contact;
	$inputs = Input::all();

	foreach($inputs as $key => $value)
	{
		$contact->$key = Input::get($key);
	}

	$contact->save();

	return Redirect::route('list-contact');
});

Route::get('/list-contact', array('as' => 'list-contact', function(){
	$contacts = Contact::all();
	$menu = Config::get('navbar.pages');
    return View::make('list-contact')->with('contacts', $contacts)->with('menu', $menu);
}));

Route::get('/contact/edit/{id}', function($id){
		$menu = Config::get('navbar.pages');
		$contact = Contact::find($id);
		return View::make('edit-contact')->with('contact', $contact)->with('menu', $menu);
});

Route::post('/contact/edit/{id}', function($id){
		$menu = Config::get('navbar.pages');
		$contact = Contact::find($id);
		$inputs = Input::all();

		foreach($inputs as $key => $value)
		{
			$contact->$key = Input::get($key);
		}

		$contact->save();

		return Redirect::route('list-contact');
});

Route::post('/list-contact/delete/{id}', function($id){
		$menu = Config::get('navbar.pages');
		$contact = Contact::find($id);
		$contact->delete();
		return Redirect::route('list-contact');
});
