@extends('layout')

@section('content')


<div class="row dark-blue white-text">
    <div class="card-header col-12">
        <h4>Editer un contact</h4>
    </div>
</div>

<form method="post">
    <center>

        </br></br>

        <div class="md-form">
            <input type="text" id="input_lastname" name="firstname" value="{{ $contact->firstname }}" class="form-control"/>
            <label for="input_firstname">Prénom</label>
        </div>

        <div class="md-form">
            <input type="text" id="input_lastname" name="lastname" value="{{ $contact->lastname }}" class="form-control"/>
            <label for="input_lastname">Nom</label>
        </div>

        <div class="md-form">
            <input type="text" name="subject" id="input_subject" value="{{ $contact->subject }}" class="form-control"/>
            <label for="input_subject">Sujet</label>
        </div>

        <textarea type="text" name="message" id="form107" class="md-textarea form-control" rows="5">{{ $contact->message }}</textarea>

        <input type="submit" value="Send" class="btn btn-primary blue"/>

    </center>
</form>

@stop
