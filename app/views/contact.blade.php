@extends('layout')

@section('content')

<center>
    <div class="card" style="max-width:500px">
        <h3 class="card-header dark-blue white-text">Contactez nous !</h3>
        <div class="card-body">
            <form method="post">
                <center>

                    </br></br>

                    <div class="md-form">
                        <input type="text" name="firstname" id="input_firstname" class="form-control">
                        <label for="input_mail">Prénom</label>
                    </div>

                    <div class="md-form">
                        <input type="text" id="input_lastname" name="lastname" class="form-control">
                        <label for="input_lastname">Nom</label>
                    </div>

                    <div class="md-form">
                        <input type="text" name="subject" id="input_subject" class="form-control">
                        <label for="input_subject">Sujet</label>
                    </div>


                    <textarea type="text" name="message" id="form107" class="md-textarea form-control" rows="5" placeholder="Votre message.."></textarea>

                    <input type="submit" value="Envoyer" class="btn btn-primary blue"/>

                </center>
            </form>
        </div>
    </div>
</center>


@stop
