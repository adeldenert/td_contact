@extends('layout')

@section('content')
<center>
          <table class="table">
            <thead class="dark-blue white-text">
              <tr>
                <div class="row">
                    <th scope="col" class="col-2">Nom</th>
                    <th scope="col" class="col-2">Prénom</th>
                    <th scope="col" class="col-2">Sujet</th>
                    <th scope="col" class="col-4">Message</th>
                    <th scope="col" class="col-1"><center>Modifier</center></th>
                    <th scope="col" class="col-1"><center>Supprimer</center></th>
                </div>
              </tr>
            </thead>

            <tbody>

              @foreach($contacts as $contact)
                  <tr>
                    <td>{{$contact['lastname']}}</td>
                    <td>{{$contact['firstname']}}</td>
                    <td>{{$contact['subject']}}</td>
                    <td>{{$contact['message']}}</td>
                    <td>
                        <a href="contact/edit/{{$contact['id']}}">
                  			<button type="submit"  class="btn btn-primary blue"/><i class="fa fa-pencil" style="color:white" aria-hidden="true"\></i></button>
                  		</a>
                    </td>
                    <td>
                        <form method="post" action="list-contact/delete/{{$contact['id']}}">
                		    <button type="submit" class="btn btn-primary red"/>
                                <i class="fa fa-trash"style="color:white" aria-hidden="true"\></i>
                            </button>
                        </form>

                    </td>
                @endforeach

            </tbody>
            </form>
          </table>
</center>

@stop
