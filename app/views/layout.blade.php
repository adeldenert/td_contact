

<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Contact Manager</title>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Bootstrap core CSS -->
        <link href="{{ asset('/assets/mdb/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="{{ asset('/assets/mdb/css/mdb.min.css') }}" rel="stylesheet">
        <!-- Your custom styles (optional) -->
        <link href="{{ asset('/assets/mdb/css/style.css') }}" rel="stylesheet">

    </head>

    <center>
        <body class="body">

            <div class="card layout-card">
                <h3 class="card-header dark-blue white-text"><center>Contact Manager</center></h3>
                <div class="card-body">

                    <div class="row">

                    @foreach($menu as $key => $element)

                    	<div class="col-6">
                    		<center>
                    			<a class="nav-link" href="{{ url($key) }}">
                    				<input value="{{$element}}" class="btn btn-primary {{$key}} blue"/>
                    				</br>
                    			</a>
                    		</center>
                    	</div>

                    @endforeach

                    </div>

                    </br>

                    @yield('content')

                </div>

            </div>
        </div>
    </center>

            <!-- JQuery -->
            <script type="text/javascript" src="{{ asset('/assets/mdb/js/jquery-3.3.1.min.js') }}"></script>
            <!-- Bootstrap tooltips -->
            <script type="text/javascript" src="{{ asset('/assets/mdb/js/popper.min.js') }}"></script>
            <!-- Bootstrap core JavaScript -->
            <script type="text/javascript" src="{{ asset('/assets/mdb/js/bootstrap.min.js') }}"></script>
            <!-- MDB core JavaScript -->
            <script type="text/javascript" src="{{ asset('/assets/mdb/js/mdb.min.js') }}"></script>

    </body>

</html>
