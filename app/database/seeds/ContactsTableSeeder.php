<?php

class ContactsTableSeeder extends Seeder {
  public function run() {
    DB::table('contacts')->insert(
      array(
        array(
          'firstname' => 'Antoine',
          'lastname' => 'Lol',
          'subject' => 'lol',
          'message' => 'Je suis trop rigolo.',
        ),
        array(
          'firstname' => 'Jean-Edouard',
          'lastname' => 'Dufour',
          'subject' => 'Yes',
          'message' => 'un message pour Jean Eud !',
        ),
        array(
          'firstname' => 'Louis',
          'lastname' => 'Seize',
          'subject' => 'Ma tête',
          'message' => 'Je la trouve plus.',
        ),
        array(
          'firstname' => 'Paul',
          'lastname' => 'Malart',
          'subject' => 'tp laravel',
          'message' => 'Putain j\'y arrive pas ma vie c\'est trop de la merde :\'(',
        )
      )
    );
  }
}
